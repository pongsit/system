To start a new project
1. copy every thing in root/ to the root of your project the files under root/ shoud be
app/ vendor/ composer.json composer.lock index.php
2. cd to app/ and run "mv gitignore .gitignore"
3. create database in phpmyadmin and copy and paste the sql command in root/app/system/db.php from line 2 to the end of the file
4. edit root/app/system/conf.php and fill out the info (at least database info)
5. create firebase project get var firebaseConfig and firebase version then paste to root/app/system/conf.php
6. enable firebase authentication on google
7. go to root from a browser and login with google