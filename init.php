<?php
	
session_start();

// get path info
$url = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$urls = parse_url($url);
$pathinfos = pathinfo($urls['path']);
$urls = array_merge($urls,$pathinfos);
$current_url = $url;
$urls['current'] = $current_url;
$host = $urls['scheme'].'://'.$urls['host'].$urls['dirname'];
$urls['host'] = $host;
$dirnames = explode('/',$urls['dirname']);
// Array ( [0] => ajnunu2 [1] => vendor [2] => pongsit [3] => auth ) 

// force use core
$use_core = 0;
if(!empty($_GET['use_core'])){
	$use_core = +$_GET['use_core'];
}

// get baseurl
$dirnames = array_values(array_filter($dirnames, function($value) { return !empty($value); }));
$baseurl = '/';
$_parent_count = 0;
foreach($dirnames as $k => $v){
	if($v=='core' || $v=='app' || $v=='vendor'){
		break;
	}
	$_parent_count++;
	$baseurl .= $v.'/';
}

// get path to root
$path_to_root = '';
$_count = count($dirnames);
$_count = $_count-$_parent_count;
if(empty($pathinfos['extension'])){
	$_count++;
}

for($i=$_count;$i>0;$i--){
	$path_to_root .= '../';
}
$path_to_core = $path_to_root.'vendor/pongsit/';
$path_to_app = $path_to_root.'app/';
$path_to_vendor = $path_to_root.'vendor/';

// get query string
$query_string = '';
if(!empty($urls['query'])){
	$query_string = '?'.$urls['query'];
}

$doc_root = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].'/'.$baseurl);

// get config
if(file_exists($doc_root.'app/system/conf.php')){
	require_once($doc_root.'app/system/conf.php');
}else{
	require_once $doc_root.'vendor/pongsit/system/conf.php';
}
$confVariables['baseurl'] = $baseurl;
$_app = basename($urls['dirname']);
$app = '';
$app_path = '';
if(!($_app == 'core' || $_app == 'app')){
	$app = $_app;
	$app_path = $_app.'/';
}
$confVariables['app'] = $app;
$confVariables['path_to_root'] = $path_to_root;
$confVariables['path_to_core'] = $path_to_core;
$confVariables['path_to_app'] = $path_to_app;
$confVariables['path_to_vendor'] = $path_to_vendor;
$confVariables['current_url']=$current_url;

// redirect from core if app exists
$path_basename = $urls['basename'];
if(in_array('pongsit',$dirnames)){
	if(file_exists($path_to_app.$app_path.$path_basename) && $use_core==0){
		header('Location:'.$path_to_app.$app_path.$path_basename.$query_string);
		exit();
	}
}
 
require_once $doc_root.'vendor/autoload.php'; // Autoload files using Composer autoload

spl_autoload_register(function($className){	
	$class_path = '';
	$app_php_path = $GLOBALS['path_to_app'].$className.'/';
	if(file_exists($app_php_path.'class/'.$className.'.php')){ 
		$class_path = $app_php_path.'class/'.$className.'.php';
	}
	if(!empty($class_path)){
		if(file_exists($class_path)){
			require_once $class_path;
		}
	}else{
		error_log('ไม่พบ '.$className.' ซึ่งเป็น Class ที่จำเป็นต้องใช้');
	}
});

if(!empty($confVariables['https']) && !isset($_SERVER['HTTPS'])){
	if (substr($_SERVER['HTTP_HOST'], 0, 4) === 'www.') {
		header('Location: https://'.substr($_SERVER['HTTP_HOST'], 4).$_SERVER['REQUEST_URI']);
	}else{
		header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	}
}

if(empty($confVariables['db'])){
	// $confVariables['db']['hostname'] = 'localhost';
	// $confVariables['db']['username'] = 'root';
	// $confVariables['db']['password'] = 'root';
	// $confVariables['db']['database'] = 'database';
	echo 'Please set up config for db. See the code for more detail.';
	exit();
}

$db = new \pongsit\dbmy\dbmy($confVariables['db']['hostname'],$confVariables['db']['username'],$confVariables['db']['password'],$confVariables['db']['database']);

if ($db->connect_error) {
	echo 'Please update database configuration.';
	exit();
}

$confVariables['style'] = '';
if(file_exists($path_to_core.'system/css/style.css')){
	$confVariables['style'] .= '
<link rel="stylesheet" href="'.$path_to_core.'system/css/style.css">';
}
if(file_exists($path_to_app.'system/css/style.css')){
	$confVariables['style'] .= '
<link rel="stylesheet" href="'.$path_to_app.'system/css/style.css">';
}

$time = new \pongsit\time\time();
$now = $time->now('YmdHis');

$site = new \pongsit\site\site();
$site_infos = $site->get_info();

$confVariables['site_name'] = '';
if(!empty($site_infos['name'])){
	$confVariables['site_name'] = $site_infos['name'];
}
$confVariables['site_description'] = '';
if(!empty($site_infos['description'])){
	$confVariables['site_description'] = $site_infos['description'];
}
$confVariables['site_author'] = '';
if(!empty($site_infos['author'])){
	$confVariables['site_author'] = $site_infos['author'];
}
$confVariables['site_type'] = 'Organization';
if(!empty($site_infos['type'])){
	$confVariables['site_type'] = $site_infos['type'];
}
$confVariables['favicon'] = $site->get_img('favicon');
$confVariables['logo'] = $site->get_img('logo');
$confVariables['google-search-result-image'] = $site->get_img('google-search-result-image');


$confVariables['profile_image']=$path_to_core.'system/img/profile/male-avatar.png';
if(!empty($_SESSION['user']['id'])){
	if(file_exists($path_to_root.'app/system/img/profile/'.$_SESSION['user']['id'])){
		$confVariables['profile_image']=$path_to_root.'app/system/img/profile/'.$_SESSION['user']['id'];
	}
}

if(empty($_SESSION['home'])){
	$confVariables['home'] = 'index.php';
}else{
	$confVariables['home'] = $_SESSION['home'];
}

if(!empty($confVariables['line']['at'])){
	$confVariables['line_at_link'] = 'https://line.me/R/ti/p/'.$confVariables['line']['at'];
}

$auth = new \pongsit\auth\auth();
$auth->lock($confVariables['auth']);
$authVariables = $auth->variables;

$view = new \pongsit\view\view();
$role = new \pongsit\role\role();

$confVariables['menu-role'] = '';
$confVariables['menu-user'] = '';
$confVariables['menu-super-admin'] = '';
$confVariables['menu-everyone'] = '';
if(!empty($_SESSION['user']['username']) && !empty($_SESSION['user']['id'])){	
	$confVariables['username'] = $_SESSION['user']['username'];
	$confVariables['user_id'] = $_SESSION['user']['id'];
	
	$confVariables['profile-image'] = $path_to_core.'system/img/profile/male-avatar.png';
	if(!empty(file_exists($path_to_root.'app/img/profile/'.$_SESSION['user']['id']))){
		$confVariables['profile-image'] = $path_to_root.'app/img/profile/'.$_SESSION['user']['id'];
	}
	
	$roles = $role->get_all_role_info_for($_SESSION['user']['id']);
	foreach($roles as $vs){
		$vs = array_merge($vs,$confVariables);
		$confVariables['menu-role'] .= $view->block('menu-'.$vs['name'], $vs);
	}
	
	if(!$role->check('user')){
		$confVariables['menu-user'] = $view->block('menu-user', $confVariables);
	}
	
	if($_SESSION['user']['id']==1){
		$confVariables['menu-super-admin'] = $view->block('menu-super-admin', $confVariables);
	}
	$confVariables['menu-everyone'] = $view->block('menu-everyone', $confVariables);
}

$confVariables['menu-search'] = '';
$confVariables['image_version'] = '';


 