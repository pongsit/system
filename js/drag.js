// get all the objects with drag class
var drag = $('.drop-area');

// when a drag enter those objects
drag.on('dragenter', function (e){
	// prevents the event from bubbling up the DOM tree
	e.stopPropagation();
	// prevents the defualt browser behavior
	e.preventDefault();
});

// when the drag is over the objects
drag.on('dragover', function (e){
	// prevents the event from bubbling up the DOM tree
	e.stopPropagation();
	// prevents the defualt browser behavior
	e.preventDefault();
	// change the background color to gray
	$(this).css('background-color', 'rgb(235,235,235)');
});

// when the drag leave the body
drag.on('dragleave', function (e){
	// prevents the event from bubbling up the DOM tree
	e.stopPropagation();
	// prevents the defualt browser behavior
	e.preventDefault();
	// change the background color to white
	$(this).css('background-color', 'white');
});

// when the file(s) are dropped to the dragcontainer
drag.on('drop', function (e){
	// prevent default behavior  
	e.preventDefault();
	var files = e.originalEvent.dataTransfer.files;
	// change the backgroud color back to white
	$(this).css('background-color', 'white');
	// สร้าง drag_done function มาเพื่อใช้หลังจากที่ได้ Files และ drop area แล้ว
	if (typeof drag_done == "function") drag_done($(this),files);
});
