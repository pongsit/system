$(function(){
	// when a drag enter those objects
	$('body').on('click tap','.show_hide', function (e){
		var $this_id = $(this).attr('id');
		var $show = $(this).data('show');
		var $hide = $(this).data('hide');
		var $toggle = $(this).data('toggle');
		if($show){
			$('.'+$this_id).show();
		}else if($hide){
			$('.'+$this_id).hide();
		}else{
			$('.'+$this_id).toggle();
		}
	});
});