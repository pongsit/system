<?php

require_once __DIR__ . '/../vendor/pongsit/system/init.php';

/* custom each page body style*/
// $confVariables['body_style'] = '
// background:rgb(0,77,129) url('.$path_to_app.'site/img/background.png) no-repeat center center fixed;
// -webkit-background-size:cover;
// -moz-background-size:cover;
// -o-background-size:cover;
// background-size:cover;';

$variables['page-name'] = 'Welcome';
$variables['body'] = 'It is nice to see you.';

echo $view->create($variables);