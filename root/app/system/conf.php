<?php

$confVariables = array();

/* database */
$confVariables['db']['hostname'] = 'localhost';
$confVariables['db']['username'] = '';
$confVariables['db']['password'] = '';
$confVariables['db']['database'] = '';

/* firebase */
$confVariables['firebase']['firebaseConfig'] = '

'; // get this info from firebase; var firebaseConfig
$confVariables['firebase']['version'] = '';
$confVariables['firebase'] = '
<script src="https://www.gstatic.com/firebasejs/'.$confVariables['firebase']['version'].'/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/'.$confVariables['firebase']['version'].'/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/'.$confVariables['firebase']['version'].'/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/'.$confVariables['firebase']['version'].'/firebase-analytics.js"></script>
<script>
  '.$confVariables['firebase']['firebaseConfig'].'
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script>';

/* custom font*/
$confVariables['font'] = "
@font-face {
  font-family: THSarabun;
  src: url('{{path_to_vendor}}pongsit/system/fonts/THSarabun/THSarabun.ttf');
}";

/* custom body style*/
$confVariables['body_style'] = '
color:white;
background:rgb(0,0,0) url('.$path_to_app.'site/img/background.png) no-repeat center center fixed;
-webkit-background-size:cover;
-moz-background-size:cover;
-o-background-size:cover;
background-size:cover;';

/* if you want all the traffic to go to https, uncomment the following line */
// $confVariables['https'] = true;

/* authentication */
$confVariables['auth'] = true;
$confVariables['auth_skip'] = array('google.php','facebook.php','phone.php','line-login-redirect.php');

/* line login */
$confVariables['line']['client_id'] = '';
$confVariables['line']['redirect_uri'] = '';
$confVariables['line']['state'] = '';
$confVariables['line']['secret'] = '';

/* line notify */
$confVariables['linenotify']['client_id'] = '';
$confVariables['linenotify']['client_secret'] = '';
$confVariables['linenotify']['line_api_register'] = 'https://notify-bot.line.me/oauth/authorize';
$confVariables['linenotify']['line_api_callback'] = 'https://notify-bot.line.me/oauth/token';
$confVariables['linenotify']['line_api_push'] = 'https://notify-api.line.me/api/notify';
$confVariables['linenotify']['callback_uri'] = 'http://localhost'.$baseurl.'vendor/pongsit/linenotify/callback.php';
$confVariables['linenotify']['system'][] = '';

/* firestore*/
$confVariables['firestore']['key'] = '';
$confVariables['firestore']['email'] = '';
$confVariables['firestore']['password'] = '';
$confVariables['firestore']['project_id'] = '';

/* linepay */
$confVariables['linepay']['channelId'] = '';
$confVariables['linepay']['channelSecret'] = '';
$confVariables['linepay']['confirm'] = 'http://localhost'.$baseurl.'app/payment/confirm.php';
$confVariables['linepay']['cancel'] = 'http://localhost'.$baseurl.'app/payment/cancel.php';
$confVariables['linepay']['isSandbox'] = true; // use false in production

/* paypal */
$confVariables['paypal']['clientId'] = '';
$confVariables['paypal']['clientSecret'] = '';
$confVariables['paypal']['isSandbox'] = true; // use false in production

/* scb */
$confVariables['scb']['clientId'] = '';
$confVariables['scb']['clientSecret'] = '';
$confVariables['scb']['ppId'] = '';
$confVariables['scb']['ref3'] = '';
$confVariables['scb']['terminalId'] = '';
$confVariables['scb']['merchantId'] = '';
$confVariables['scb']['isSandbox'] = true; // use false in production

